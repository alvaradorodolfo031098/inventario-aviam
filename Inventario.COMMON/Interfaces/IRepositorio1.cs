﻿using Inventario.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inventario.COMMON.Interfaces
{
   public interface IRepositorio1<T>where T:Base
    {
        bool Create(T entidad);
        bool Update(T EntidadModificada);
        bool Delete(string Id);
        List<T> Read { get; }
    }
}
