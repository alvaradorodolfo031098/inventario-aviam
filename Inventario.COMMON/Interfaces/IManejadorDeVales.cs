﻿using Inventario.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Inventario.COMMON.Interfaces
{
   public interface IManejadorDeVales:IManejadorGenerico<Vale>
    {
        List<Vale> ValesPorLiquidar();
        List<Vale> ValesEnIntervalo(DateTime Inicio, DateTime Fin);
        IEnumerable BuscarNoEmtregadoPorEmpleado(Empleado empleado);
    }
}
