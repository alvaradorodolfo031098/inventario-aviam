﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventario.COMMON.Entidades;

namespace Inventario.COMMON.Interfaces
{
    public class ManejadorDeVales : IManejadorDeVales
    {
        IRepositorio1<Vale> repositorio;
        public ManejadorDeVales(IRepositorio1<Vale> repositorio)
        {
            this.repositorio = repositorio;
        }
        public List<Vale> Listar => repositorio.Read;


        public bool Agregar(Vale Entidad)

        {
            return repositorio.Create(Entidad);
        }

        public IEnumerable BuscarNoEmtregadoPorEmpleado(Empleado empleado)
        {
            return repositorio.Read.Where(p => p.Solicitante.Id == empleado.Id && p.FechaEntregaReal == null);
        }

        public Vale BuscarPorId(string Id)
        {
            return Listar.Where(e => e.Id == Id).SingleOrDefault();
        }

        public bool Eliminar(string id)
        {
            return repositorio.Delete(id);
        }

        public bool Modificar(Vale entidad)
        {
            return repositorio.Update(entidad);
        }

        public List<Vale> ValesEnIntervalo(DateTime Inicio, DateTime Fin)
        {
            throw new NotImplementedException();
        }

        public List<Vale> ValesPorLiquidar()
        {
            throw new NotImplementedException();
        }
    }
}

