﻿using Inventario.BIZ;
using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using Inventario.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Invetario.GUI.Almacen
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum Accion
        {
            Nuevo,
            Edita
        }
        ManejadorDeVales manejadorDeVales;
        ManejadorEmpleados manejadorDeEmpleados;
        ManejadorPiezas manejadorDePiezas;
        Vale vale;

        Accion accionDeVale;

        public MainWindow()
        {
            InitializeComponent();
            manejadorDeVales = new ManejadorDeVales(new RepositorioDeVales());
            manejadorDeEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorDePiezas = new ManejadorPiezas(new RepositorioDePiezas());


            ActualizarTablaDeVales();
            GridDetalle.IsEnabled = false;
        }

        private void ActualizarTablaDeVales()
        {
            dtgVales.ItemsSource = null;
            dtgVales.ItemsSource = manejadorDeVales.Listar;
               
        }

        private void btnNuevoVale_Click(object sender, RoutedEventArgs e)
        {
            GridDetalle.IsEnabled = true;
            ActualizarMisCombosDetalle();
            vale = new Vale();
            vale.PiezasPrestados = new List<Piezas>();
            ActualizarListaDePiezasEnMiVale();
            accionDeVale = Accion.Nuevo;
        }

        private void ActualizarListaDePiezasEnMiVale()
        {
            dtgPiezasEnVale.ItemsSource = null;
            dtgPiezasEnVale.ItemsSource = vale.PiezasPrestados;
        }

        private void ActualizarMisCombosDetalle()
        {
            cmbAlmacenista.ItemsSource = null;
            cmbAlmacenista.ItemsSource = manejadorDeEmpleados.EmpleadosPorArea("Almacen");

            cmbPiezas.ItemsSource = null;
            cmbPiezas.ItemsSource = manejadorDePiezas.Listar;

            cmbSolicitante.ItemsSource = null;
            cmbSolicitante.ItemsSource=manejadorDeEmpleados.Listar;   
        }

        private void btnEliminarVale_Click(object sender, RoutedEventArgs e)
        {
            Vale v = dtgVales.SelectedItem as Vale;
            if(v != null)
            {
                if(MessageBox.Show("Realmente deseas eliminar el vale", "Almacen", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (manejadorDeVales.Eliminar(v.Id))
                    {
                        MessageBox.Show("Eliminado con exito", "Almacen", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                        ActualizarTablaDeVales();
                    }
                    else
                    {
                        MessageBox.Show("Algo salio mal", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
            }
        }

        private void btnAgregarPieza_Click(object sender, RoutedEventArgs e)
        {
            Piezas m = cmbPiezas.SelectedItem as Piezas;
            if (m != null)
            {
                vale.PiezasPrestados.Add(m);
                ActualizarListaDePiezasEnMiVale();
            }
        }

        private void btnEliminarPieza_Click(object sender, RoutedEventArgs e)
        {
            Piezas m = dtgPiezasEnVale.SelectedItem as Piezas;
            if (m != null)
            {
                vale.PiezasPrestados.Remove(m);
                ActualizarListaDePiezasEnMiVale();
            }
        }

        private void btnGuardarVale_Click(object sender, RoutedEventArgs e)
        {
            if(accionDeVale== Accion.Nuevo)
            {
                vale.EncargadoDeEntrega = cmbAlmacenista.SelectedItem as Empleado;
                vale.FechaEntrega = dtpFechaEmtrega.SelectedDate.Value;
                vale.FechaHoraSolicitud = DateTime.Now;
                vale.Solicitante = cmbSolicitante.SelectedItem as Empleado;
                vale.FechaEntregaReal = DateTime.Parse(lblFechaHoraEntregaReal.Content.ToString());
                if (manejadorDeVales.Modificar(vale))
                {
                    MessageBox.Show("Vale guardado con exito","Almacen",MessageBoxButton.OK,MessageBoxImage.Exclamation);
                    LimpiarLosCamposDeVale();
                    GridDetalle.IsEnabled = false;
                    ActualizarTablaDeVales();
                }
                else
                {
                    MessageBox.Show("Error al guardar el vale", "Almacen", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                vale.EncargadoDeEntrega = cmbAlmacenista.SelectedItem as Empleado;
                vale.FechaEntrega = dtpFechaEmtrega.SelectedDate.Value;
                vale.FechaHoraSolicitud = DateTime.Now;
                vale.Solicitante = cmbSolicitante.SelectedItem as Empleado;

            }
        }

        private void LimpiarLosCamposDeVale()
        {
            dtpFechaEmtrega.SelectedDate = DateTime.Now;
            lblFechaHoraEntregaReal.Content = "";
            lblFechaHoraSolicitud.Content = "";
            dtgPiezasEnVale.ItemsSource = null;
            cmbAlmacenista.ItemsSource = null;
            cmbPiezas.ItemsSource = null;
            cmbSolicitante.ItemsSource = null;
        }

        private void dtgVales_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Vale v = dtgVales.SelectedItem as Vale;
            if (v != null)
            {
                GridDetalle.IsEnabled = true;
                vale = v;
                ActualizarListaDePiezasEnMiVale();
                accionDeVale = Accion.Edita;
                lblFechaHoraSolicitud.Content = vale.FechaHoraSolicitud.ToString();
                lblFechaHoraEntregaReal.Content = vale.FechaEntregaReal.ToString();
                ActualizarMisCombosDetalle();
                cmbAlmacenista.Text = vale.EncargadoDeEntrega.ToString();
                cmbSolicitante.Text = vale.Solicitante.ToString();
                dtpFechaEmtrega.SelectedDate = vale.FechaEntrega;
            }
        }

        private void btnEmtregarVale_Click(object sender, RoutedEventArgs e)
        {
            lblFechaHoraEntregaReal.Content = DateTime.Now;
        }

        private void btnCancelarVale_Click(object sender, RoutedEventArgs e)
        {
            LimpiarLosCamposDeVale();
            GridDetalle.IsEnabled = false;
        }

        private void btnReportes_Click(object sender, RoutedEventArgs e)
        {
            Reportes reportes = new Reportes();
            reportes.Show();
        }
    }
}
