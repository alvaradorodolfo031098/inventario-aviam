﻿using Inventario.BIZ;
using Inventario.COMMON.Entidades;
using Inventario.COMMON.Interfaces;
using Inventario.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Inventario.GUI.Administrador
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum accion
        {
            Nuevo,
            Editar
        }
        IManejadorEmpleados ManejadorEmpleados;
        IManejadorPiezas manejadorPiezas;

        accion accionEmpleados;
        accion accionPiezas;
        public MainWindow()
        {
            InitializeComponent();

            ManejadorEmpleados = new ManejadorEmpleados(new RepositorioDeEmpleados());
            manejadorPiezas = new ManejadorPiezas(new RepositorioDePiezas());
            PonerBotonesEmpleadoEnEdicion(false);
            LimpiarCamposDeEmpleados();
            ActualizarTablaEmpleados();

            PonerBotonesPiezasEnEdicion(false);
            LimpiarCamposDePiezas();
            ActualizarTablaDePiezas();

        }

        private void ActualizarTablaDePiezas()
        {
            dtgPiezas.ItemsSource = null;
            dtgPiezas.ItemsSource = manejadorPiezas.Listar;

        }

        private void LimpiarCamposDePiezas()
        {
            txbPiezasCategoria.Clear();
            txbPiezasDescripcion.Clear();
            txbPiezasId.Text = "";
            txbPiezasNombre.Clear();
        }

        private void PonerBotonesPiezasEnEdicion(bool value)
        {
            btnPiezasCancelar.IsEnabled = value;
            btnPiezasEditar.IsEnabled = !value;
            btnPiezasEliminar.IsEnabled = !value;
            btnPiezasGuardar.IsEnabled = value;
            btnPiezasNuevo.IsEnabled = !value;
        }

        private void ActualizarTablaEmpleados()
        {
            dtgEmpleados.ItemsSource = null;
            dtgEmpleados.ItemsSource = ManejadorEmpleados.Listar;
        }

        private void LimpiarCamposDeEmpleados()
        {
            txbEmpleadoApellido.Clear();
            txbEmpleadoArea.Clear();
            txbEmpleadoId.Text = "";
            txbEmpleadoNombre.Clear();
        }

        private void PonerBotonesEmpleadoEnEdicion(bool value)
        {
            btnEmpleadoCancelar.IsEnabled = value;
            btnEmpleadoEditar.IsEnabled = !value;
            btnEmpleadoEliminar.IsEnabled = !value;
            btnEmpleadoGuardar.IsEnabled = value;
            btnEmpleadoNuevo.IsEnabled = !value;
        }

        private void btnEmpleadoNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadoEnEdicion(true);
            accionEmpleados = accion.Nuevo;
        }

        private void btnEmpleadoGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionEmpleados == accion.Nuevo)
            {
                Empleado emp = new Empleado()
                {
                    Nombre = txbEmpleadoNombre.Text,
                    Apellido = txbEmpleadoApellido.Text,
                    Area = txbEmpleadoArea.Text

                };
                if (ManejadorEmpleados.Agregar(emp))
                {
                    MessageBox.Show("Empleado agregado corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadoEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El empleado no se pudo agregar ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Empleado emp = dtgEmpleados.SelectedItem as Empleado;
                emp.Apellido = txbEmpleadoApellido.Text;
                emp.Area = txbEmpleadoArea.Text;
                emp.Nombre = txbEmpleadoNombre.Text;
                if (ManejadorEmpleados.Modificar(emp))
                {
                    MessageBox.Show("Empleado modificado corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDeEmpleados();
                    ActualizarTablaEmpleados();
                    PonerBotonesEmpleadoEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("El empleado no se pudo actualizar ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnEmpleadoCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDeEmpleados();
            PonerBotonesEmpleadoEnEdicion(false);
        }

        private void btnEmpleadoEliminar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                if (MessageBox.Show("Realmente deseas eliminar este empleado?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    if (ManejadorEmpleados.Eliminar(emp.Id))
                    {
                        MessageBox.Show("Empleado eliminado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                        ActualizarTablaEmpleados();
                    }
                    else
                    {
                        MessageBox.Show("No se pudo eliminar el empleado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    }
                }
            }
        }

        private void btnEmpleadoEditar_Click(object sender, RoutedEventArgs e)
        {
            Empleado emp = dtgEmpleados.SelectedItem as Empleado;
            if (emp != null)
            {
                txbEmpleadoId.Text = emp.Id;
                txbEmpleadoApellido.Text = emp.Apellido;
                txbEmpleadoArea.Text = emp.Area;
                txbEmpleadoNombre.Text = emp.Nombre;
                accionEmpleados = accion.Editar;
                PonerBotonesEmpleadoEnEdicion(true);
            }
        }

        private void btnPiezasNuevo_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            accionPiezas = accion.Nuevo;
            PonerBotonesPiezasEnEdicion(true);
        }

        private void btnPiezasEditar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            accionPiezas = accion.Editar;
            PonerBotonesPiezasEnEdicion(true);
            Piezas m = dtgPiezas.SelectedItem as Piezas;
            if (m != null)
            {
                txbPiezasCategoria.Text = m.Categoria;
                txbPiezasDescripcion.Text = m.Descripcion;
                txbPiezasId.Text = m.Id;
                txbPiezasNombre.Text = m.Nombre;
            }
        }

        private void btnPiezasGuardar_Click(object sender, RoutedEventArgs e)
        {
            if (accionPiezas == accion.Nuevo)
            {
                Piezas m = new Piezas()
                {
                    Categoria = txbPiezasCategoria.Text,
                    Descripcion = txbPiezasDescripcion.Text,
                    Nombre = txbPiezasNombre.Text

                };
                if (manejadorPiezas.Agregar(m))
                {
                    MessageBox.Show("Pieza agregada corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDePiezas();
                    ActualizarTablaDePiezas();
                    PonerBotonesPiezasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                Piezas m = dtgEmpleados.SelectedItem as Piezas;
                m.Categoria = txbPiezasCategoria.Text;
                m.Descripcion = txbPiezasDescripcion.Text;
                m.Nombre = txbPiezasNombre.Text;
                if (manejadorPiezas.Modificar(m))
                {
                    MessageBox.Show("Pieza corectamente Modificado", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    LimpiarCamposDePiezas();
                    ActualizarTablaDePiezas();
                    PonerBotonesPiezasEnEdicion(false);
                }
                else
                {
                    MessageBox.Show("Algo salio mal ", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        private void btnPiezasCancelar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            PonerBotonesPiezasEnEdicion(false);
        }

        private void btnPiezasEliminar_Click(object sender, RoutedEventArgs e)
        {
            LimpiarCamposDePiezas();
            PonerBotonesPiezasEnEdicion(false);
            Piezas m = dtgPiezas.SelectedItem as Piezas;
            if (MessageBox.Show("Realmente deseas eliminar esta pieza?", "Inventarios", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                if (manejadorPiezas.Eliminar(m.Id))
                {
                    MessageBox.Show("Piezas eliminado corectamente", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                    ActualizarTablaDePiezas();
                }
                else
                {
                    MessageBox.Show("No se pudo eliminar las piezas", "Inventarios", MessageBoxButton.OK, MessageBoxImage.Information);
                }
            }
        }
    }
  }
